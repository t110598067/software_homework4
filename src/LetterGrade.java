import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LetterGrade {
    public static void main(String[] args) {

        System.out.print("Enetr the score = ");
        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            int score = Integer.parseInt(br.readLine());
            char grade = letterGrade(score);
            System.out.println("The grade of " + score + " is " + grade);
        } catch (NumberFormatException ex) {
            System.out.println("Not an integer!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
        public static char letterGrade(int score) {
        char grade;
        if (score < 0 || score > 100) grade = 'X';
        else if (score >= 90 && score <= 100) grade = 'A';
        else if (score >= 80 && score < 90) grade = 'B';
        else if (score >= 70 && score < 80) grade = 'C';
        else if (score >= 60 && score < 70) grade = 'D';
        else
            grade = 'F';
        return grade;
    }
    @Test
    public void test5a(){
        Assert.assertEquals('X',letterGrade(-1));
        Assert.assertEquals('X',letterGrade(101));
        Assert.assertEquals('F',letterGrade(0));
        Assert.assertEquals('F',letterGrade(59));
        Assert.assertEquals('D',letterGrade(60));
        Assert.assertEquals('D',letterGrade(69));
        Assert.assertEquals('C',letterGrade(70));
        Assert.assertEquals('C',letterGrade(79));
        Assert.assertEquals('B',letterGrade(80));
        Assert.assertEquals('B',letterGrade(89));
        Assert.assertEquals('A',letterGrade(90));
        Assert.assertEquals('A',letterGrade(100));
    }
    @Test
    public void test5b(){
        Assert.assertEquals('X',letterGrade(-1));
        Assert.assertEquals('X',letterGrade(101));
        Assert.assertEquals('F',letterGrade(0));
        Assert.assertEquals('F',letterGrade(30));
        Assert.assertEquals('F',letterGrade(59));
        Assert.assertEquals('D',letterGrade(60));
        Assert.assertEquals('D',letterGrade(65));
        Assert.assertEquals('D',letterGrade(69));
        Assert.assertEquals('C',letterGrade(70));
        Assert.assertEquals('C',letterGrade(75));
        Assert.assertEquals('C',letterGrade(79));
        Assert.assertEquals('B',letterGrade(80));
        Assert.assertEquals('B',letterGrade(85));
        Assert.assertEquals('B',letterGrade(89));
        Assert.assertEquals('A',letterGrade(90));
        Assert.assertEquals('A',letterGrade(95));
        Assert.assertEquals('A',letterGrade(100));
    }
}
